INSERT INTO z_city
	VALUES ('500152', 'Brasov'),
	('500285', 'Sibiu'),
	('400152', 'Constanta');

INSERT INTO z_articles
	VALUES (1, 'Paste Carbonara',32,9),
	(2, 'Pizza', 28, 9),
	(3,'Burger',26,9);

INSERT INTO z_adress 
	VALUES (1,'1 Decembrie 1918','500152',3),
	(2,'Calea Bucuresti', '500152',52),
	(3,'Bulevardul Victorie','400152',88);

INSERT INTO z_restaurant 
	VALUES (1,'McDonalds','0745896562','0745896562',2),
	(2,'KFC', '0722365895','0722365895',1),
	(3,'Spartan','0744589641','0744589641',1);

INSERT INTO z_person
	VALUES (1,'Robert','Bucur','m',1),
	(2,'Dode','Bogdan','m',3),
	(3,'Gabi','Burtan','f',2);

INSERT INTO z_customer 
	VALUES (1),
	(2),
	(3);

INSERT INTO z_employee
	VALUES (1),
	(2),
	(3);

INSERT INTO z_invoice 
	VALUES (1,'2019-12-11','10:00',1,1,1),
	(2,'2018-12-01','6:00',1,2,1),
	(3,'2019-12-01','11:00',2,2,2);

INSERT INTO z_invoice_articles
	VALUES (1,1,12.5,9),
	(1,2,22,9),
	(1,3,32,9);