CREATE TABLE Z_City(
	Zip varchar(6) primary key NOT NULL, 
	City varchar(10) NOT NULL);
CREATE TABLE Z_Articles(
	Article_ID int primary key NOT NULL,
	Arcticle_Name varchar(15) NOT NULL, 			   
	Price float NOT NULL, TVA float NOT NULL);
CREATE TABLE Z_Adress(
	Adress_ID int primary key, 
	Street varchar(20) NOT NULL,					 
	Zip varchar(6) REFERENCES Z_City(Zip), 
	House_Number int);
CREATE TABLE Z_Restaurant(
	U_ID int primary key NOT NULL, 
	Restaurant_Name varchar(20) NOT NULL,						 
	Phone varchar(10), 
	Fax varchar(10), 
	Adress_ID int REFERENCES Z_Adress(Adress_ID));
CREATE TABLE Z_Person(
	P_ID int primary key NOT NULL, 
	Surname varchar(10),					 
	First_Name varchar(10) NOT NULL, 
	Sex char, 
	Adress_ID int REFERENCES Z_Adress(Adress_ID));
CREATE TABLE Z_Customer(
	P_ID int primary key REFERENCES Z_Person(P_ID));
CREATE TABLE Z_Employee(
	P_ID int primary key REFERENCES Z_Person(P_ID));
CREATE TABLE Z_Invoice(
	Invoice_No int primary key NOT NULL, 
	Invoice_Date Date, Invoice_Time Time, 
	P_ID_EMP int REFERENCES Z_Employee(P_ID),					 
	P_ID_CUSTOMER int REFERENCES Z_Customer(P_ID), 
	U_ID int REFERENCES Z_Restaurant(U_ID));
CREATE TABLE Z_Invoice_Articles(
	Invoice_No int REFERENCES Z_Invoice(Invoice_No), 
	Article_ID int REFERENCES Z_Articles(Article_ID),
	Price float, 
	Tva float);