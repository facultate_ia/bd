SELECT denumire, sum(cantitate) as suma from produs
inner join comandaprodus on produs.prodid=comandaprodus.prodid
group by denumire
order by suma desc limit 1;

SELECT denumire from produs
WHERE prodid NOT IN
(SELECT prodid from comandaprodus);

SELECT firma,
COUNT(comanda.clientid) as cate_comenzi
FROM client INNER JOIN comanda ON client.clientid=comanda.clientid
WHERE comanda.datalivrare>current_date - interval '6 months'
GROUP BY firma LIMIT 1;

SELECT* FROM comanda
WHERE datalivrare<current_date;
