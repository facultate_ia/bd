CREATE TABLE Adresa(
	CodPostal INT PRIMARY KEY NOT NULL,
	Strada VARCHAR(20),
	Localitate VARCHAR(10)
);

CREATE TABLE Client(
	ClientID INT PRIMARY KEY NOT NULL,
	Firma VARCHAR(20),
	Status VARCHAR(10),
	Adresa INT REFERENCES Adresa(CodPostal)
);

CREATE TABLE Produs(
	ProdID INT PRIMARY KEY NOT NULL,
	Denumire VARCHAR(20),
	Pret FLOAT
);

CREATE TABLE Comanda(
	ComandaID INT PRIMARY KEY NOT NULL,
	DataComanda DATE,
	DataLivrare DATE,
	ClientID INT REFERENCES Client(ClientID)
);

CREATE TABLE ComandaProdus(
	ComandaID INT REFERENCES Comanda(ComandaID),
	ProdID INT REFERENCES Produs(ProdID),
	Cantitate INT,
	PretTotal FLOAT
);

CREATE TABLE Depozit(
	DepozitID INT PRIMARY KEY NOT NULL,
	Localitate VARCHAR(10),
	Strada VARCHAR(20)
);

CREATE TABLE Produs_Depozit(
	ProdID INT REFERENCES Produs(ProdID),
	DepozitID INT REFERENCES Depozit(DepozitID),
	Cantitate FLOAT
)


