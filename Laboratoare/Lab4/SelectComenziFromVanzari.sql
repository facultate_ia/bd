SELECT firma
FROM comanda inner join client
on comanda.clientid=client.clientid
where client.clientid=2;

SELECT firma, sum(prettotal)
FROM comanda inner join comandaprodus
ON comanda.comandaid=comandaprodus.comandaid
inner join client
on comanda.clientid=client.clientid
group by firma;

SELECT denumire
FROM produs inner join produs_depozit
on produs.prodid=produs_depozit.prodid
where cantitate=0;