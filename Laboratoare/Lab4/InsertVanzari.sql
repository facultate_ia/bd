INSERT INTO adresa 
	VALUES ('507080','Mihai Viteazul','Halchiu'),
	('500258','Calea Feldioarei', 'Brasov'),
	('400526','Calea Victoriei', 'Bucuresti');

INSERT INTO client 
	VALUES (1,'Waters','angajat','507080'),
	(2,'Cerner','angajat', '500258'),
	(3,'Codegile','angajat','400526');

INSERT INTO produs 
	VALUES (1,'Laptop',2500),
	(2,'Telefon',4800),
	(3,'Televizor',1500);

INSERT INTO comanda 
	VALUES (1,'2019-11-22','2019-11-26',1),
	(2,'2019-11-29','2019-11-2',2),
	(3,'2019-10-11','2019-11-01',1);

INSERT INTO comandaprodus 
	VALUES (1,1,3,7500),
	(2,1,1,2500),
	(3,1,2,500);

INSERT INTO depozit 
	VALUES (1,'Brasov','Calea Feldioarei'),
	(2,'Brasov','Bartolomeu Nord'),
	(3,'Bucuresti','Calea Victoriei');

INSERT INTO produs_depozit 
	VALUES (1,1,25),
	(1,2,12),
	(2,1,1);