SELECT r_name FROM Restaurant;

SELECT COUNT(r_name) FROM Restaurant;

SELECT COUNT(r_name) FROM Restaurant
	JOIN Address ON Restaurant.adr_id = Address.adr_id
	JOIN City ON Address.zip = City.zip
	WHERE City.city = 'Brasov';
	
SELECT * FROM Person
	WHERE birthdate > '31.12.1899' AND birthdate < '01.01.2000';
	
SELECT * FROM City;

SELECT * FROM City
	WHERE city LIKE 'B%';
