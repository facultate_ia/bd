CREATE TABLE Person
(
    p_id INT NOT PRIMARY KEY NULL,
    surname VARCHAR(20),
    first_name VARCHAR(20),
    sex VARCHAR(5),
    birthdate DATE,
    adr_id INT REFERENCES Address(adr_id)
)