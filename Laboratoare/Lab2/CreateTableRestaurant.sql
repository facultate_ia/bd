CREATE TABLE Restaurant
(
    r_id INT PRIMARY KEY NOT NULL,
    r_name VARCHAR(20),
    phone VARCHAR(10),
    fax VARCHAR(10),
    adr_id INT REFERENCES Address(adr_id)
)