CREATE TABLE Address
(
    adr_id INT PRIMARY KEY NOT NULL,
    street VARCHAR(20),
    zip INT REFERENCES City(zip),
    house_number VARCHAR(5)
)