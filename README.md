# Materiale Baze de Date

## !!! [Ghidul de proiectare](Ghid de proiectare a bazelor de date.pdf) ajuta la proiect, dar mai ales la examen !!!

- [Ghidul de proiectare](Ghid de proiectare a bazelor de date.pdf) este cel mai important document, ajuta super mult la examen
- [Intrebari de teorie](Intrebari teorie) sunt cele din laboratoare la care trebuie raspuns si apoi incarcate pe platforma
- [Laboratoare](Laboratoare) sunt interogarile SQL de la laboratoare
- [Partiale](Partiale) sunt intrebarile trimise de mine pentru partial
- [Proiect](Proiect) este proiectul facut de mine la BD pe etape